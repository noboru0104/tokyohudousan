$(function(){
	"use strict";
	ImgChange();
	ImgHoverChange();
	HeaderHeight();
	//SPMenuHeight();
	//MenuHeight();
	
	// TOGGLE NAV	
	$( "#global-nav-area" ).hide();
	var flg = 'default';
	$(document).on("click","#toggle, #global-nav .p-close", function() {
	//$( "#toggle" ).click( function(){

		$( "#toggle-btn-icon" ).toggleClass( "close" );

		if( flg === "default" ){
			
			flg = "close";

			$( "#global-nav-area" ).addClass( "show" );
			//setTimeout(function(){
//				$('.global-list li a').matchHeight();
//			},10);

		}else{
			
			flg = "default";
			$( "#global-nav-area" ).removeClass( "show" );
			//$('.global-list li a').off();
		}

		$("#global-nav-area").slideToggle('slow');
	});
	
	//$( ".js-global-nav-detail" ).hide();
	//$( ".js-underlayer" ).click(
//		function(){
//			$(this).next().toggleClass( "show" );
//	
//			if( $(this).next().hasClass( "show" ) ){
//				
//				$(this).find(".js-menu").text( "―" );
//				$(this).next().addClass( "show" );
//	
//			}else{
//				
//				$(this).find(".js-menu").text( "＋" );
//				$(this).next().removeClass( "show" );
//			}
//	
//			$(this).next().slideToggle('slow');
//		}
//	);
	
	var topBtn = $('#page-top');    
	topBtn.hide();
	
	//スクロールが500に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 100) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
		
		//TOP以外のページの時にスクロールが一番上にある時ヘッダーの背景の透明度を無しにする。
		if($('#top').length){
			
		} else {
			
			//if ($(window).scrollTop() < 75) {
			//	$(".l-header").css("background-color","rgba(0,0,0,1.0)");
			//} else {
			//	$(".l-header").css("background-color","rgba(0,0,0,0.5)");
			//}
		}
	});
	
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 1500);
		return false;
	});
	
	$(window).on('load', function(){
		
	});	
		
	$('a[href^=#]').click(function(){	
		if($('#company').length){
			
		} else {
			var speed = 1000;
			var href= $(this).attr("href");
			var target = $(href === "#" || href === "" ? 'html' : href);
			var position = 0;
			
			if($(this).hasClass('is-pagescroll')){
				position = target.offset().top - $('.l-header').height();				
				$("html, body").animate({scrollTop:position}, speed, "swing");
				return false;
			} else if($(this).hasClass('is-concept')){
			
			} else {
				position = target.offset().top;			
				$("html, body").animate({scrollTop:position}, speed, "swing");
				return false;
			}
		}
	});
	
	$(".p-info").hover(
		function(){
			//alert('a');
			$(this).find('.header_courses_menu').addClass('on_hover');
		},
		function () {
			$(this).find('.header_courses_menu').removeClass('on_hover');
		}
	);
	
	//$(".js-globalMenu").hover(
//		function(){
//			$(this).next('.header_courses_menu').addClass('on_hover');
//		},
//		function () {
//			$(this).next('.header_courses_menu').removeClass('on_hover');
//		}
//	);
//	
//	$(".header_courses_menu").hover(
//		function(){
//			$(this).addClass('on_hover');
//		},
//		function () {
//			$(this).removeClass('on_hover');
//		}
//	);


//------------モーダル↓---------------------------------

	$(document).on("click","#modal-open, #modal-open2, #modal-open3, .is-link", function() {
	//------------------------------------------------------------------
	// キーボード操作などにより、オーバーレイが多重起動するのを防止する
	//------------------------------------------------------------------
    // ボタンからフォーカスを外す
        $(this).blur();
    // 新しくモーダルウィンドウを起動しない
        if($("#modal-overlay")[0]){ return false;}
    // 現在のモーダルウィンドウを削除して新しく起動する
        // if($("#modal-overlay")[0]) $("#modal-overlay").remove();
    // オーバーレイ用のHTMLコードを、[body]内の最後に生成する
        $("body").append('<div id="modal-overlay"></div>');
    // #modal-overlay 及び #modalcontent をフェードインさせる
        $("#modal-overlay, #modal-content").fadeIn("slow");
        centeringModalSyncer();

	//------------------------------------------------------------------
	// #modal-overlay または #modal-close のクリック時に実行する処理
	//------------------------------------------------------------------
        $("#modal-overlay, #modal-close").off().on("click", function() {
        // #modal-overlay 及び #modal-close をフェードアウトする
            $("#modal-content, #modal-overlay").fadeOut("slow", function() {
            // フェードアウト後、 #modal-overlay をHTML(DOM)上から削除
                $("#modal-overlay").remove();
            });
        });

	//------------------------------------------------------------------
	// リサイズ操作をした際に、モーダルウィンドウを中央寄せにする
	//------------------------------------------------------------------
    // Case.1 リサイズ操作の度に実行する場合
        // $(window).resize(centeringModalSyncer);
    // Case.2 リサイズ操作が終了したときのみ実行する場合
        var timer = false;
        $(window).resize(function() {
            if (timer !== false) clearTimeout(timer);
            timer = setTimeout(function() {
                centeringModalSyncer();
            }, 200);
        });

    //------------------------------------------------------------------
    // モーダルウィンドウを中央寄せする関数
    //------------------------------------------------------------------
        function centeringModalSyncer() {
        // 画面(ウィンドウ)の幅、高さを取得
            var w = $(window).width();
            var h = window.innerHeight;

        // コンテンツ(#modal-content)の幅、高さを取得
        // Case.1 margin 含める場合
            // var cw = $("#modal-content").outerWidth(true);
            // var ch = $("#modal-content").outerHeight(true);
        // Case.2 margin 含めない場合
            var cw = $("#modal-content").outerWidth();
            var ch = $("#modal-content").outerHeight();

        // #modal-content を真ん中に配置するのに、左端と上部から何ピクセル離せばいいか？を計算してCSSのポジションを設定する
        // Case.1 left と top で変数を分ける
        /*
            var pxleft = ((w - cw) / 2);
            var pxtop  = ((h - ch) / 2);
            $("#modal-content").css({
                "left":pxleft + "px",
                "top":pxtop + "px"
            });
        */
        // Case.2 プロパティを持たせて一つの変数に纏める
			var p_prop = {
				left:((w - cw) / 2),
				top:((h - ch) / 2)
			};
            $("#modal-content").css(p_prop);
        }
    });
	
//------------モーダル↑---------------------------------
	
	$(window).on('load resize', function(){
		ImgChange();
		ImgHoverChange();
		HeaderHeight();
		setTimeout(function(){
			SPMenuHeight();
		},100);
					
	});
	
	if(navigator.userAgent.indexOf('Android') > 0){
        
    }
	
});


function  ImgChange(){
	"use strict";
	// スマホ画像切替
	var widimage = window.innerWidth;
	//var widimage = parseInt($(window).width()) + 16;
	
	if( widimage < 1001 ){
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
		
		$('.is-imgChange').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 769 ){
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange2').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}
	
	if( widimage < 481 ){
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_pc', '_sp'));
		});
	} else {
	
		$('.is-imgChange3').each( function(){
			$(this).attr("src",$(this).attr("src").replace('_sp', '_pc'));
		});
	}

}
function  ImgHoverChange(){
	"use strict";
	
	$('.is-hoverChange').hover(function(){
		$(this).attr("src",$(this).attr("src").replace('_off', '_on'));		
	},function(){
		$(this).attr("src",$(this).attr("src").replace('_on', '_off'));		
	});
	
	var fadeSpeed = 300;
	var rolloverImg = $('.is-hoverChange2');
	rolloverImg.each(function() { 
	
		//ブラウザサイズによる画像の切り替え
		var widimage = window.innerWidth;
		if( widimage < 1001 ){
			$('.is-hoverChange2').each( function(){
				//$(this).attr("src",$(this).attr("src").replace('_off', '_on'));
				//$(this).attr("src",$(this).attr("src").replace('_on', '_off'));
			});
		} else {
			//フェードインで画像を切り替え
			
			if(this.src.match('_off')) {
			var imgWidth = $(this).css('width');
			var imgHeight = 'auto';
				$(this).parent('a').css( {display: 'block', width: imgWidth + 'px'});
				 
				this.onImgSrc = new Image();
				this.onImgSrc.src = this.getAttribute('src').replace('_off', '_on'); 
				$(this.onImgSrc).css( {position: 'absolute', opacity: 0, width: imgWidth + 'px', height: 'auto'} ); 
				$(this).before(this.onImgSrc);
				 
				$(this.onImgSrc).mousedown(function(){ 
					$(this).stop().animate({opacity: 0}, {duration: fadeSpeed, queue: false}); 
				}); 
		 
				$(this.onImgSrc).hover(
					function(){ $(this).animate( {opacity: 1}, {duration: fadeSpeed, queue: false}); },
					function(){ $(this).animate( {opacity: 0}, {duration: fadeSpeed, queue: false}); }
				);
			}
		}
	});
	
	var fadeSpeed2 = 200;
	var rolloverImg2 = $('.is-hoverChange3');
	rolloverImg2.each(function() { 
	
		//ブラウザサイズによる画像の切り替え
		var widimage = window.innerWidth;
		if( widimage < 1001 ){
			$('.is-hoverChange3').each( function(){
				//$(this).attr("src",$(this).attr("src").replace('_off', '_on'));
				//$(this).attr("src",$(this).attr("src").replace('_on', '_off'));
			});
		} else {
			//フェードインで画像を切り替え
			if(this.src.match('_off')) {
			var imgWidth2 = $(this).css('width');
			var imgHeight2 = 'auto';
				$(this).parent('a').css( {display: 'block', width: imgWidth2 + 'px', float: 'left',marginLeft:35 + 'px', verticalAlign:'top'});
				 
				this.onImgSrc2 = new Image();
				this.onImgSrc2.src = this.getAttribute('src').replace('_off', '_on'); 
				$(this.onImgSrc2).css( {position: 'absolute', opacity: 0, width: imgWidth2 + 'px', height: 'auto'} ); 
				$(this).before(this.onImgSrc2);
				 
				$(this.onImgSrc2).mousedown(function(){ 
					$(this).stop().animate({opacity: 0}, {duration: fadeSpeed2, queue: false}); 
				}); 
		 
				$(this.onImgSrc2).hover(
					function(){ $(this).animate( {opacity: 1}, {duration: fadeSpeed2, queue: false}); },
					function(){ $(this).animate( {opacity: 0}, {duration: fadeSpeed2, queue: false}); }
				); 
			}
		}
	});
}

function  HeaderHeight(){
	"use strict";
	var header_Height = $('header').height();
	$('.l-mvBlock').css('marginTop',header_Height + 'px');
}
function  SPMenuHeight(){
	"use strict";
	var header_Height = $('header').height();
	var menu_Height = window.innerHeight - 70;
	//var menu_Height = window.innerHeight - 150;
	$('#global-nav-area').css('top',header_Height + 'px');
	$('#global-nav-area').css('height',menu_Height + 'px');
	$('#global-nav-area').css('overflow-y','auto');
	$('#global-nav-area').css('-webkit-overflow-scrolling','touch');
}


